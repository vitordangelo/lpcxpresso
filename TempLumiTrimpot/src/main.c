/*****************************************************************************
 * Faça um programa para mostrar no display OLED o valor da temperatura
 * ambiente e o nível de luminosidade do ambiente.
 *
 * O programa deve também ler o valor do Trimpot e mostrar no display de 7
 * segmentos a letra “A” quando o valor for acima da metade e a letra “b”
 * quando for abaixo. Quando o valor for a metade mostrar o número “0”.
 ******************************************************************************/

/*
 Como foi proposto no exercício mostrar a letra A quando o trimpot estiver acima da metade,
 b quando estiver abaixo da metade e zero quando estiver no meio.
 Imprimi os valores lidos pelo trimpot e analisei o valor máximo e mínimo, dividindo o intervalo entre eles por 2.
 Portanto é dificil acertar o valor do "meio" para mostrar zero no display
 */

#include "lpc17xx_pinsel.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_timer.h"

#include "light.h"
#include "oled.h"
#include "temp.h"
#include "led7seg.h"

static uint32_t msTicks = 0;
static uint8_t buf[10];

static void intToString(int value, uint8_t* pBuf, uint32_t len, uint32_t base) {
	static const char* pAscii = "0123456789abcdefghijklmnopqrstuvwxyz";
	int pos = 0;
	int tmpValue = value;

	// the buffer must not be null and at least have a length of 2 to handle one
	// digit and null-terminator
	if (pBuf == NULL || len < 2) {
		return;
	}

	// a valid base cannot be less than 2 or larger than 36
	// a base value of 2 means binary representation. A value of 1 would mean only zeros
	// a base larger than 36 can only be used if a larger alphabet were used.
	if (base < 2 || base > 36) {
		return;
	}

	// negative value
	if (value < 0) {
		tmpValue = -tmpValue;
		value = -value;
		pBuf[pos++] = '-';
	}

	// calculate the required length of the buffer
	do {
		pos++;
		tmpValue /= base;
	} while (tmpValue > 0);

	if (pos > len) {
		// the len parameter is invalid.
		return;
	}

	pBuf[pos] = '\0';

	do {
		pBuf[--pos] = pAscii[value % base];
		value /= base;
	} while (value > 0);

	return;

}

void SysTick_Handler(void) {
	msTicks++;
}

static uint32_t getTicks(void) {
	return msTicks;
}

static void init_ssp(void) {
	SSP_CFG_Type SSP_ConfigStruct;
	PINSEL_CFG_Type PinCfg;

	/*
	 * Initialize SPI pin connect
	 * P0.7 - SCK;
	 * P0.8 - MISO
	 * P0.9 - MOSI
	 * P2.2 - SSEL - used as GPIO
	 */
	PinCfg.Funcnum = 2;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 7;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 8;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 9;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Funcnum = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 2;
	PINSEL_ConfigPin(&PinCfg);

	SSP_ConfigStructInit(&SSP_ConfigStruct);

	// Initialize SSP peripheral with parameter given in structure above
	SSP_Init(LPC_SSP1, &SSP_ConfigStruct);

	// Enable SSP peripheral
	SSP_Cmd(LPC_SSP1, ENABLE);

}

static void init_i2c(void) {
	PINSEL_CFG_Type PinCfg;

	/* Initialize I2C2 pin connect */
	PinCfg.Funcnum = 2;
	PinCfg.Pinnum = 10;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 11;
	PINSEL_ConfigPin(&PinCfg);

	// Initialize I2C peripheral
	I2C_Init(LPC_I2C2, 100000);

	/* Enable I2C1 operation */
	I2C_Cmd(LPC_I2C2, ENABLE);
}

static void init_adc(void) {
	PINSEL_CFG_Type PinCfg;

	/*
	 * Init ADC pin connect
	 * AD0.0 on P0.23
	 */
	PinCfg.Funcnum = 1;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Pinnum = 23;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);

	/* Configuration for ADC :
	 * 	Frequency at 1Mhz
	 *  ADC channel 0, no Interrupt
	 */
	ADC_Init(LPC_ADC, 1000000);
	ADC_IntConfig(LPC_ADC, ADC_CHANNEL_0, DISABLE);
	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_0, ENABLE);

}

int main(void) {

	uint8_t ch = '0';

	int32_t t = 0;
	uint32_t lux = 0;
	uint32_t trim = 0;

	init_i2c();
	init_ssp();
	init_adc();

	oled_init();
	light_init();

	temp_init(&getTicks);

	led7seg_init();

	if (SysTick_Config(SystemCoreClock / 1000)) {
		while (1)
			;  // Capture error
	}

	/*
	 * Assume base board in zero-g position when reading first value.
	 */

	light_enable();
	light_setRange(LIGHT_RANGE_4000);

	oled_clearScreen(OLED_COLOR_WHITE);

	oled_putString(1, 1, (uint8_t*) "Temp   : ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);
	oled_putString(1, 9, (uint8_t*) "Light  : ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);
	oled_putString(1, 17, (uint8_t*) "Trimpot: ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);

	while (1) {
		//Temepratura
		t = temp_read();

		//Luminosidade
		lux = light_read();

		//Trimpot - Leitura - ADC
		ADC_StartCmd(LPC_ADC, ADC_START_NOW);
		//Wait conversion complete
		while (!(ADC_ChannelGetStatus(LPC_ADC, ADC_CHANNEL_0, ADC_DATA_DONE)))
			;
		trim = ADC_ChannelGetData(LPC_ADC, ADC_CHANNEL_0);

		//Imprime os valores no displey
		intToString(t, buf, 10, 10);
		oled_fillRect((1 + 9 * 6), 1, 80, 8, OLED_COLOR_WHITE);
		oled_putString((1 + 9 * 6), 1, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);

		intToString(lux, buf, 10, 10);
		oled_fillRect((1 + 9 * 6), 9, 80, 16, OLED_COLOR_WHITE);
		oled_putString((1 + 9 * 6), 9, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);

		intToString(trim, buf, 10, 10);
		oled_fillRect((1 + 9 * 6), 17, 80, 24, OLED_COLOR_WHITE);
		oled_putString((1 + 9 * 6), 17, buf, OLED_COLOR_BLACK,
				OLED_COLOR_WHITE);

		/* delay */
		Timer0_Wait(200);

		//Atribuiu o valor no displey de 7s conforme o valor do trimpot
		//Valores para plotar no 7s são dados em ASCII
		if (trim > 1994) {
			ch = 65;
			led7seg_setChar(ch, FALSE);
		}

		if (trim < 1994) {
			ch = 98;
			led7seg_setChar(ch, FALSE);
		}

		if (trim == 1994) {
			ch = 0;
			led7seg_setChar(0, FALSE);
		}

	}

}

void check_failed(uint8_t *file, uint32_t line) {
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
		;
}
