Exercicio – Utilizando diversos periféricos
--------------------------------------------

Faça um programa para mostrar no display OLEDo valor da temperatura ambiente e o nível de luminosidade do ambiente.

O programa deve também ler o valor do Trimpot e mostrar no display de 7 segmentos a letra “A” quando o valor for acima da metade e a letra “b” 
quando for abaixo. Quando o valor for a metade mostrar o número “0”.

Configurar o display OLED para trabalhar com fundo branco e letras azuis
