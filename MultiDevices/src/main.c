/*****************************************************************************
 * Faça um programa que use, através de uma aplicação de sua escolha,
 * que use pelo menos 5 dispositivos da placa BaseBoard, sendo que destes
 * 5, no mínimo 2 precisam ser diferentes dos já utilizados anteriormente.
 ******************************************************************************/

#include "lpc17xx_pinsel.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_timer.h"

#include "light.h"
#include "oled.h"
#include "temp.h"
#include "acc.h"
#include "rgb.h"

static uint32_t msTicks = 0;
static uint8_t buf[10];

//Converte inteiro para string
static void intToString(int value, uint8_t* pBuf, uint32_t len, uint32_t base) {
	static const char* pAscii = "0123456789abcdefghijklmnopqrstuvwxyz";
	int pos = 0;
	int tmpValue = value;

	// the buffer must not be null and at least have a length of 2 to handle one
	// digit and null-terminator
	if (pBuf == NULL || len < 2) {
		return;
	}

	// a valid base cannot be less than 2 or larger than 36
	// a base value of 2 means binary representation. A value of 1 would mean only zeros
	// a base larger than 36 can only be used if a larger alphabet were used.
	if (base < 2 || base > 36) {
		return;
	}

	// negative value
	if (value < 0) {
		tmpValue = -tmpValue;
		value = -value;
		pBuf[pos++] = '-';
	}

	// calculate the required length of the buffer
	do {
		pos++;
		tmpValue /= base;
	} while (tmpValue > 0);

	if (pos > len) {
		// the len parameter is invalid.
		return;
	}

	pBuf[pos] = '\0';

	do {
		pBuf[--pos] = pAscii[value % base];
		value /= base;
	} while (value > 0);

	return;

}

void SysTick_Handler(void) {
	msTicks++;
}

static uint32_t getTicks(void) {
	return msTicks;
}

//Inicializa SSP
static void init_ssp(void) {
	SSP_CFG_Type SSP_ConfigStruct;
	PINSEL_CFG_Type PinCfg;

	/*
	 * Initialize SPI pin connect
	 * P0.7 - SCK;
	 * P0.8 - MISO
	 * P0.9 - MOSI
	 * P2.2 - SSEL - used as GPIO
	 */
	PinCfg.Funcnum = 2;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 7;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 8;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 9;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Funcnum = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 2;
	PINSEL_ConfigPin(&PinCfg);

	SSP_ConfigStructInit(&SSP_ConfigStruct);

	// Initialize SSP peripheral with parameter given in structure above
	SSP_Init(LPC_SSP1, &SSP_ConfigStruct);

	// Enable SSP peripheral
	SSP_Cmd(LPC_SSP1, ENABLE);

}

//Inicializa I2C
static void init_i2c(void) {
	PINSEL_CFG_Type PinCfg;

	/* Initialize I2C2 pin connect */
	PinCfg.Funcnum = 2;
	PinCfg.Pinnum = 10;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 11;
	PINSEL_ConfigPin(&PinCfg);

	// Initialize I2C peripheral
	I2C_Init(LPC_I2C2, 100000);

	/* Enable I2C1 operation */
	I2C_Cmd(LPC_I2C2, ENABLE);
}

//Inicializa ADC
static void init_adc(void) {
	PINSEL_CFG_Type PinCfg;

	/*
	 * Init ADC pin connect
	 * AD0.0 on P0.23
	 */
	PinCfg.Funcnum = 1;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Pinnum = 23;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);

	/* Configuration for ADC :
	 * 	Frequency at 1Mhz
	 *  ADC channel 0, no Interrupt
	 */
	ADC_Init(LPC_ADC, 1000000);
	ADC_IntConfig(LPC_ADC, ADC_CHANNEL_0, DISABLE);
	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_0, ENABLE);

}

int main(void) {
	//Posição inicial do acelerometro
	int32_t xoff = 0;
	int32_t yoff = 0;
	int32_t zoff = 0;

	int8_t x = 0;
	int8_t y = 0;
	int8_t z = 0;

	//Temperatura
	int32_t t = 0;

	//Luminosidade
	uint32_t lux = 0;

	//Trimpot
	uint32_t trim = 0;

	//Inicializa interface
	init_i2c();
	init_ssp();
	init_adc();

	//Inicializa device
	oled_init();
	light_init();
	acc_init();

	temp_init(&getTicks);

	rgb_init();

	if (SysTick_Config(SystemCoreClock / 1000)) {
		while (1)
			;  // Capture error
	}

	//Configuração do acelerometro
	acc_read(&x, &y, &z);
	xoff = 0 - x;
	yoff = 0 - y;
	zoff = 64 - z;

	light_enable();
	light_setRange(LIGHT_RANGE_4000);

	//Limpa display
	oled_clearScreen(OLED_COLOR_WHITE);

	//Imprime o "rótulo" no display
	oled_putString(1, 1, (uint8_t*) "Temp   : ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);
	oled_putString(1, 9, (uint8_t*) "Light  : ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);
	oled_putString(1, 17, (uint8_t*) "Trimpot: ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);
	oled_putString(1, 25, (uint8_t*) "Acc x  : ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);
	oled_putString(1, 33, (uint8_t*) "Acc y  : ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);
	oled_putString(1, 41, (uint8_t*) "Acc z  : ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);

	while (1) {

		//Atribui os valores do acelerometro a variável
		acc_read(&x, &y, &z);
		x = x + xoff;
		y = y + yoff;
		z = z + zoff;

		uint8_t r = 0;
		uint8_t g = 0;
		uint8_t b = 0;

		//Temperatura
		t = temp_read();

		//Luminosidade
		lux = light_read();

		//Trimpot
		ADC_StartCmd(LPC_ADC, ADC_START_NOW);
		//Wait conversion complete
		while (!(ADC_ChannelGetStatus(LPC_ADC, ADC_CHANNEL_0, ADC_DATA_DONE)))
			;
		trim = ADC_ChannelGetData(LPC_ADC, ADC_CHANNEL_0);

		//Imprime valores no display
		intToString(t, buf, 10, 10);
		oled_fillRect((1 + 9 * 6), 1, 80, 8, OLED_COLOR_WHITE);
		oled_putString((1 + 9 * 6), 1, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);

		intToString(lux, buf, 10, 10);
		oled_fillRect((1 + 9 * 6), 9, 80, 16, OLED_COLOR_WHITE);
		oled_putString((1 + 9 * 6), 9, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);

		intToString(trim, buf, 10, 10);
		oled_fillRect((1 + 9 * 6), 17, 80, 24, OLED_COLOR_WHITE);
		oled_putString((1 + 9 * 6), 17, buf, OLED_COLOR_BLACK,
				OLED_COLOR_WHITE);

		intToString(x, buf, 10, 10);
		oled_fillRect((1 + 9 * 6), 25, 80, 32, OLED_COLOR_WHITE);
		oled_putString((1 + 9 * 6), 25, buf, OLED_COLOR_BLACK,
				OLED_COLOR_WHITE);

		intToString(y, buf, 10, 10);
		oled_fillRect((1 + 9 * 6), 33, 80, 40, OLED_COLOR_WHITE);
		oled_putString((1 + 9 * 6), 33, buf, OLED_COLOR_BLACK,
				OLED_COLOR_WHITE);

		intToString(z, buf, 10, 10);
		oled_fillRect((1 + 9 * 6), 41, 80, 48, OLED_COLOR_WHITE);
		oled_putString((1 + 9 * 6), 41, buf, OLED_COLOR_BLACK,
				OLED_COLOR_WHITE);

		//Atribuí os valores do acelerometro ao RGB
		r = x;
		g = y;
		b = z;

		if (t < 17) {
			oled_putString(1, 50, (uint8_t*) "FRIO", OLED_COLOR_BLACK,
					OLED_COLOR_WHITE);
		}

		if ((t > 17) & (t < 23)) {
			oled_putString(1, 50, (uint8_t*) "FRESCO", OLED_COLOR_BLACK,
					OLED_COLOR_WHITE);
		}

		if (t > 23) {
					oled_putString(1, 50, (uint8_t*) "CALOR", OLED_COLOR_BLACK,
							OLED_COLOR_WHITE);
				}

		x = RGB_RED;
		y = RGB_GREEN;
		z = RGB_BLUE;

		rgb_setLeds(r | g | b);

		/* delay */
		Timer0_Wait(200);
	}

}

void check_failed(uint8_t *file, uint32_t line) {
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
		;
}
